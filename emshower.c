#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include<TLeaf.h>
#ifndef FnuG4_h
#define FnuG4_h
class Hit: public TObject{
	public:
	int iz, izsub, pdgid, id, idParent;
	float charge, x, y, z, px, py, pz, e1, e2, len, edep;
	ClassDef(Hit, 1);
};
class FnuG4 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        e_beam;
   Int_t           id_beam;
   Double_t        x_beam;
   Double_t        y_beam;
   Int_t           pdgnu_nuEvt;
   Int_t           pdglep_nuEvt;
   Double_t        Enu_nuEvt;
   Double_t        Plep_nuEvt;
   Int_t           cc_nuEvt;
   Double_t        x_nuEvt;
   Double_t        y_nuEvt;
   Double_t        z_nuEvt;
   Int_t           nhits;
   Int_t           chamber_count;
   Int_t           chamber[846048];   //[chamber_count]
   Int_t           iz_count;
   Int_t           iz[846048];   //[iz_count]
   Int_t           izsub_count;
   Int_t           izsub[846048];   //[izsub_count]
   Int_t           pdgid_count;
   Int_t           pdgid[846048];   //[pdgid_count]
   Int_t           id_count;
   Int_t           id[846048];   //[id_count]
   Int_t           idParent_count;
   Int_t           idParent[846048];   //[idParent_count]
   Int_t           charge_count;
   Double_t        charge[846048];   //[charge_count]
   Int_t           x_count;
   Double_t        x[846048];   //[x_count]
   Int_t           y_count;
   Double_t        y[846048];   //[y_count]
   Int_t           z_count;
   Double_t        z[846048];   //[z_count]
   Int_t           px_count;
   Double_t        px[846048];   //[px_count]
   Int_t           py_count;
   Double_t        py[846048];   //[py_count]
   Int_t           pz_count;
   Double_t        pz[846048];   //[pz_count]
   Int_t           e1_count;
   Double_t        e1[846048];   //[e1_count]
   Int_t           e2_count;
   Double_t        e2[846048];   //[e2_count]
   Int_t           len_count;
   Double_t        len[846048];   //[len_count]
   Int_t           edep_count;
   Double_t        edep[846048];   //[edep_count]

   // List of branches
   TBranch        *b_row_wise_branch;   //!

	TObjArray *particles;
   


   FnuG4(TTree *tree=0);
   virtual ~FnuG4();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   void Print();
   
};

FnuG4::FnuG4(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("FASERnu_numu.dump._001.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("FASERnu_numu.dump._001.root");
      }
      f->GetObject("FASERnu",tree);

   }
   Init(tree);
   
   particles = new TObjArray;
   particles->SetOwner(1);
}

FnuG4::~FnuG4()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}
Int_t FnuG4::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   
   delete particles;
   particles = new TObjArray;
   particles->SetOwner(1);
   
   int ret = fChain->GetEntry(entry);
   nhits = chamber_count;

   return ret;
}

void FnuG4::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("row_wise_branch", &e_beam, &b_row_wise_branch);


 
   fChain->GetLeaf("e_beam")->SetAddress( &e_beam);
   fChain->GetLeaf("id_beam")->SetAddress( &id_beam);
   fChain->GetLeaf("x_beam")->SetAddress( &x_beam);
   fChain->GetLeaf("y_beam")->SetAddress( &y_beam);
   fChain->GetLeaf("pdgnu_nuEvt")->SetAddress( &pdgnu_nuEvt);
   fChain->GetLeaf("pdglep_nuEvt")->SetAddress( &pdglep_nuEvt);
   fChain->GetLeaf("Enu_nuEvt")->SetAddress( &Enu_nuEvt);
   fChain->GetLeaf("Plep_nuEvt")->SetAddress( &Plep_nuEvt);
   fChain->GetLeaf("cc_nuEvt")->SetAddress( &cc_nuEvt);
   fChain->GetLeaf("x_nuEvt")->SetAddress( &x_nuEvt);
   fChain->GetLeaf("y_nuEvt")->SetAddress( &y_nuEvt);
   fChain->GetLeaf("z_nuEvt")->SetAddress( &z_nuEvt);
//   fChain->GetLeaf("chamber_count")->SetAddress( &nhits);
   fChain->GetLeaf("chamber_count")->SetAddress( &chamber_count);
   fChain->GetLeaf("chamber")->SetAddress(chamber);   //[chamber_count]
   fChain->GetLeaf("iz_count")->SetAddress( &iz_count);
   fChain->GetLeaf("iz")->SetAddress(iz);   //[iz_count]
   fChain->GetLeaf("izsub_count")->SetAddress( &izsub_count);
   fChain->GetLeaf("izsub")->SetAddress(izsub);   //[izsub_count]
   fChain->GetLeaf("pdgid_count")->SetAddress( &pdgid_count);
   fChain->GetLeaf("pdgid")->SetAddress(pdgid);   //[pdgid_count]
   fChain->GetLeaf("id_count")->SetAddress( &id_count);
   fChain->GetLeaf("id")->SetAddress(id);   //[id_count]
   fChain->GetLeaf("idParent_count")->SetAddress( &idParent_count);
   fChain->GetLeaf("idParent")->SetAddress(idParent);   //[idParent_count]
   fChain->GetLeaf("charge_count")->SetAddress( &charge_count);
   fChain->GetLeaf("charge")->SetAddress(charge);   //[charge_count]
   fChain->GetLeaf("x_count")->SetAddress( &x_count);
   fChain->GetLeaf("x")->SetAddress(x);   //[x_count]
   fChain->GetLeaf("y_count")->SetAddress( &y_count);
   fChain->GetLeaf("y")->SetAddress(y);   //[y_count]
   fChain->GetLeaf("z_count")->SetAddress( &z_count);
   fChain->GetLeaf("z")->SetAddress(z);   //[z_count]
   fChain->GetLeaf("px_count")->SetAddress( &px_count);
   fChain->GetLeaf("px")->SetAddress(px);   //[px_count]
   fChain->GetLeaf("py_count")->SetAddress( &py_count);
   fChain->GetLeaf("py")->SetAddress(py);   //[py_count]
   fChain->GetLeaf("pz_count")->SetAddress( &pz_count);
   fChain->GetLeaf("pz")->SetAddress(pz);   //[pz_count]
   fChain->GetLeaf("e1_count")->SetAddress( &e1_count);
   fChain->GetLeaf("e1")->SetAddress(e1);   //[e1_count]
   fChain->GetLeaf("e2_count")->SetAddress( &e2_count);
   fChain->GetLeaf("e2")->SetAddress(e2);   //[e2_count]
   fChain->GetLeaf("len_count")->SetAddress( &len_count);
   fChain->GetLeaf("len")->SetAddress(len);   //[len_count]
   fChain->GetLeaf("edep_count")->SetAddress( &edep_count);
   fChain->GetLeaf("edep")->SetAddress(edep);   //[edep_count]


   Notify();
}
Bool_t FnuG4::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void FnuG4::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t FnuG4::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

#endif
void emshower(){
	FnuG4 *ev = new FnuG4();
	
	TFile f("000_FASERnu_cuts.root","recreate");
	TNtuple *z = new TNtuple("ntracks_z","","z:iz");
	TTree *params = new TTree("params","params");
	TH1D *ntrk = new TH1D("ntrk","",100,-700,200);
	TH1D *iz = new TH1D("iz","",1000,0,1000);
	TH1D *ntrk_pileup = new TH1D("ntrk_pileup","",100,-700,200);
	TH2D *ntrk_iz = new TH2D("ntrk_iz","",50,0,1000,50,10,40000);
	TH1D *iz_pileup = new TH1D("iz_pileup","iz vs. E",1000,0,1000);
	hprof = new TProfile("hprof","Profile of ntracks vs. depth",100,0,1000,0,40000);
	nprof = new TProfile("nprof","Profile of ntracks",100,0,1000,0,40000);
	nmaxprof = new TProfile("nmaxprof","Profile of ntracks",100,0,1000,0,40000);
	izprof = new TProfile("izprof","Profile of ntracks",100,0,1000,0,40000);
	
	TNtuple *nt = new TNtuple("nt","","iev:CC:Enu:x:y:z:ntracks");
	TNtuple *ntReso = new TNtuple("ntReso","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
	TNtuple *ntResoSmeared = new TNtuple("ntResoSmeared","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
                std::vector<float>* ntotal;
                std::vector<float>* nmax;
                std::vector<float>* izmax;
		params->Branch("ntotal",&ntotal);
		params->Branch("nmax",&nmax);
		params->Branch("izmax",&izmax);
		ntotal->clear();
		nmax->clear();
		izmax->clear();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(int cut1=1;cut1<20;cut1++){
		cout << "cut1 = " << cut1 << endl;
	for(int iev=0; iev<1000	; iev++){ // 3202 is electron
		if(iev==11929||iev==13543||iev==21425)continue;
		ev->GetEntry(iev);
		int nhits = ev->nhits;
		iz->Reset();
		int count = 0;
		int nmaxvalue = 0;
		int izmaxvalue = 0;
		for(int i=0; i<nhits; i++){
			if(ev->izsub[i]!=0) continue; // There are two hits for 1 emulsion film. Ignore one of them.
			if(ev->charge[i]==0) continue; // ignore neutral particles (photons, neutral hadrons) at truth level
//==========ADDITIONAL CUTS===========//
//			printf("R = %d\n",ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i]);
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>cut1/100.) continue; // ignore everything outside of 100 um cylinder (optimizing)
			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>cut1/100.*abs(ev->z[i]+700.)) continue; // ignore everything outside of 0.001 rad cone (optimizing)
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])<0.0001*abs(ev->z[i]+700.)) continue; // muon hits are close to axis, ignore them (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]>0.05) continue; // Keep only hits with track slope < 50 mrad
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]<0.0003) continue; // Muon hits have almost normal tracks, ignore (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i])<1000) continue; // Keep only hits with energy > 100 MeV
			//cout << "x = " << ev->x[i] << " y = " << ev->y[i] << endl;
			z->Fill(ev->z[i],ev->iz[i]);
			ntrk->Fill(ev->z[i]);
			ntrk_pileup->Fill(ev->z[i]);
			iz->Fill(ev->iz[i]);
			iz_pileup->Fill(ev->iz[i]);
			count++;
		}
		cout << iev << " ";
		cout << count << " ";
		nmaxvalue = iz->GetMaximum();
		cout << nmaxvalue << " ";
		izmaxvalue = iz->GetMaximumBin();
		cout << izmaxvalue << " ";
		for(int j=0;j<1000;j++){
			cout << iz->GetBinContent(j) << " ";
		}
		cout << endl;
		nprof->Fill(1,count);
		nmaxprof->Fill(1,nmaxvalue);
		izprof->Fill(1,izmaxvalue);
		ntotal->push_back(count);
		nmax->push_back(nmaxvalue);
		izmax->push_back(izmaxvalue);
		for(int i=0;i<100;i++){
			double zVal = ntrk->GetXaxis()->GetBinCenter(i);
			double izVal = iz->GetXaxis()->GetBinCenter(i);
			int nVal = ntrk->GetBinContent(i);
//			cout << "center = " << zVal << endl;
//			cout << "bin content = " << nVal << endl;
//			ntrk_z->Fill(zVal,nVal);
			ntrk_iz->Fill(izVal,nVal);
			hprof->Fill(izVal,nVal);
		}
	}
}
	params->Fill();
	params->Write();
	hprof->Write();
	nprof->Write();
	nmaxprof->Write();
	izprof->Write();
	ntrk->Write();
	iz->Write();
	ntrk_pileup->Write();
	iz_pileup->Write();
	ntrk_iz->Write();
	ntracks_z->Write();
	nt->Write();
	f.Close();
//	cout << "HITS = " << avg << endl;
//     EdbPVRec *pvr = psuedRec(ev); 	display(pvr);
//	TFile f("000_FASERnu_cuts.root");
//	TProfile *p = f.Get("nprof");
//	cout << p->GetMaximum() << endl;
	//cout << "HITS = " << t->GetBranch("nprof")->GetMaximum() << endl;
	cout << "FINISHED" << endl;

}