#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TLeaf.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <typeinfo>
#ifndef FnuG4_h
#define FnuG4_h

using namespace std;

class Hit: public TObject{
	public:
	int iz, izsub, pdgid, id, idParent;
	float charge, x, y, z, px, py, pz, e1, e2, len, edep;
	ClassDef(Hit, 1);
};
class FnuG4 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        e_beam;
   Int_t           id_beam;
   Double_t        x_beam;
   Double_t        y_beam;
   Int_t           pdgnu_nuEvt;
   Int_t           pdglep_nuEvt;
   Double_t        Enu_nuEvt;
   Double_t        Plep_nuEvt;
   Int_t           cc_nuEvt;
   Double_t        x_nuEvt;
   Double_t        y_nuEvt;
   Double_t        z_nuEvt;
   Int_t           nhits;
   Int_t           chamber_count;
   Int_t           chamber[846048];   //[chamber_count]
   Int_t           iz_count;
   Int_t           iz[846048];   //[iz_count]
   Int_t           izsub_count;
   Int_t           izsub[846048];   //[izsub_count]
   Int_t           pdgid_count;
   Int_t           pdgid[846048];   //[pdgid_count]
   Int_t           id_count;
   Int_t           id[846048];   //[id_count]
   Int_t           idParent_count;
   Int_t           idParent[846048];   //[idParent_count]
   Int_t           charge_count;
   Double_t        charge[846048];   //[charge_count]
   Int_t           x_count;
   Double_t        x[846048];   //[x_count]
   Int_t           y_count;
   Double_t        y[846048];   //[y_count]
   Int_t           z_count;
   Double_t        z[846048];   //[z_count]
   Int_t           px_count;
   Double_t        px[846048];   //[px_count]
   Int_t           py_count;
   Double_t        py[846048];   //[py_count]
   Int_t           pz_count;
   Double_t        pz[846048];   //[pz_count]
   Int_t           e1_count;
   Double_t        e1[846048];   //[e1_count]
   Int_t           e2_count;
   Double_t        e2[846048];   //[e2_count]
   Int_t           len_count;
   Double_t        len[846048];   //[len_count]
   Int_t           edep_count;
   Double_t        edep[846048];   //[edep_count]

   // List of branches
   TBranch        *b_row_wise_branch;   //!

	TObjArray *particles;
   


   FnuG4(TTree *tree=0);
   virtual ~FnuG4();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   void Print();
   
};

FnuG4::FnuG4(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("FASERnu_numu.dump._001-PILEUP.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("FASERnu_numu.dump._001-PILEUP.root");
      }
      f->GetObject("FASERnu",tree);

   }
   Init(tree);
   
   particles = new TObjArray;
   particles->SetOwner(1);
}

FnuG4::~FnuG4()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}
Int_t FnuG4::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   
   delete particles;
   particles = new TObjArray;
   particles->SetOwner(1);
   
   int ret = fChain->GetEntry(entry);
   nhits = chamber_count;

   return ret;
}

void FnuG4::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("row_wise_branch", &e_beam, &b_row_wise_branch);


 
   fChain->GetLeaf("e_beam")->SetAddress( &e_beam);
   fChain->GetLeaf("id_beam")->SetAddress( &id_beam);
   fChain->GetLeaf("x_beam")->SetAddress( &x_beam);
   fChain->GetLeaf("y_beam")->SetAddress( &y_beam);
   fChain->GetLeaf("pdgnu_nuEvt")->SetAddress( &pdgnu_nuEvt);
   fChain->GetLeaf("pdglep_nuEvt")->SetAddress( &pdglep_nuEvt);
   fChain->GetLeaf("Enu_nuEvt")->SetAddress( &Enu_nuEvt);
   fChain->GetLeaf("Plep_nuEvt")->SetAddress( &Plep_nuEvt);
   fChain->GetLeaf("cc_nuEvt")->SetAddress( &cc_nuEvt);
   fChain->GetLeaf("x_nuEvt")->SetAddress( &x_nuEvt);
   fChain->GetLeaf("y_nuEvt")->SetAddress( &y_nuEvt);
   fChain->GetLeaf("z_nuEvt")->SetAddress( &z_nuEvt);
//   fChain->GetLeaf("chamber_count")->SetAddress( &nhits);
   fChain->GetLeaf("chamber_count")->SetAddress( &chamber_count);
   fChain->GetLeaf("chamber")->SetAddress(chamber);   //[chamber_count]
   fChain->GetLeaf("iz_count")->SetAddress( &iz_count);
   fChain->GetLeaf("iz")->SetAddress(iz);   //[iz_count]
   fChain->GetLeaf("izsub_count")->SetAddress( &izsub_count);
   fChain->GetLeaf("izsub")->SetAddress(izsub);   //[izsub_count]
   fChain->GetLeaf("pdgid_count")->SetAddress( &pdgid_count);
   fChain->GetLeaf("pdgid")->SetAddress(pdgid);   //[pdgid_count]
   fChain->GetLeaf("id_count")->SetAddress( &id_count);
   fChain->GetLeaf("id")->SetAddress(id);   //[id_count]
   fChain->GetLeaf("idParent_count")->SetAddress( &idParent_count);
   fChain->GetLeaf("idParent")->SetAddress(idParent);   //[idParent_count]
   fChain->GetLeaf("charge_count")->SetAddress( &charge_count);
   fChain->GetLeaf("charge")->SetAddress(charge);   //[charge_count]
   fChain->GetLeaf("x_count")->SetAddress( &x_count);
   fChain->GetLeaf("x")->SetAddress(x);   //[x_count]
   fChain->GetLeaf("y_count")->SetAddress( &y_count);
   fChain->GetLeaf("y")->SetAddress(y);   //[y_count]
   fChain->GetLeaf("z_count")->SetAddress( &z_count);
   fChain->GetLeaf("z")->SetAddress(z);   //[z_count]
   fChain->GetLeaf("px_count")->SetAddress( &px_count);
   fChain->GetLeaf("px")->SetAddress(px);   //[px_count]
   fChain->GetLeaf("py_count")->SetAddress( &py_count);
   fChain->GetLeaf("py")->SetAddress(py);   //[py_count]
   fChain->GetLeaf("pz_count")->SetAddress( &pz_count);
   fChain->GetLeaf("pz")->SetAddress(pz);   //[pz_count]
   fChain->GetLeaf("e1_count")->SetAddress( &e1_count);
   fChain->GetLeaf("e1")->SetAddress(e1);   //[e1_count]
   fChain->GetLeaf("e2_count")->SetAddress( &e2_count);
   fChain->GetLeaf("e2")->SetAddress(e2);   //[e2_count]
   fChain->GetLeaf("len_count")->SetAddress( &len_count);
   fChain->GetLeaf("len")->SetAddress(len);   //[len_count]
   fChain->GetLeaf("edep_count")->SetAddress( &edep_count);
   fChain->GetLeaf("edep")->SetAddress(edep);   //[edep_count]


   Notify();
}
Bool_t FnuG4::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void FnuG4::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t FnuG4::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

#endif
struct Data {
    public:
        int iz_, izsub_;
        double x_, y_, z_, px_, py_, pz_;

//        bool operator<(const Data &a, const Data &b) const {
//        return a.iz_ < b.iz_;
//        }
};

void swap(Data *xp, Data *yp)
{
    Data temp = *xp;
    *xp = *yp;
    *yp = temp;
}

const vector<Data> self(vector<Data>& datavec)
{
    return datavec;
}

void emulsionreco(){
	FnuG4 *ev = new FnuG4();
	TFile *g = new TFile("basetracks.root","RECREATE");
	TH1D *hx1 = new TH1D("x1","x1",1000,-12.5,12.5);
	TH1D *hx2 = new TH1D("x2","x2",1000,-12.5,12.5);
	TH1D *hy1 = new TH1D("y1","y1",1000,-12.5,12.5);
	TH1D *hy2 = new TH1D("y2","y2",1000,-12.5,12.5);
	TH1D *hz1 = new TH1D("z1","z1",1000,-700,600);
	TH1D *hz2 = new TH1D("z2","z2",1000,-700,600);
	TH1D *hpx1 = new TH1D("px1","px1",1000,-100,100);
	TH1D *hpx2 = new TH1D("px2","px2",1000,-100,100);
	TH1D *hpy1 = new TH1D("py1","py1",1000,-100,100);
	TH1D *hpy2 = new TH1D("py2","py2",1000,-100,100);
	TH1D *hpz1 = new TH1D("pz1","pz1",1000,-100,4000);
	TH1D *hpz2 = new TH1D("pz2","pz2",1000,-100,4000);
	TH1D *hanglex = new TH1D("anglex","anglex",1000,-1.58,1.58);
	TH1D *hangley = new TH1D("angley","angley",1000,-1.58,1.58);
	TH1D *hchi2 = new TH1D("chi2","chi2",1000,0,0.1);
	for(int iev=0;iev<1;iev++){ // Loop over events, just use one for now
		ev->GetEntry(iev); // Load event
		int nhits = ev->nhits; // Get number of hits in event
		cout << "[" << iev << "] hits: " << nhits << endl;
	    vector<Data> datavector;
	    datavector.reserve(1000000);
		for(int i=0;i<nhits;i++){  // <<-------------- nhits
			int pdgid = ev->pdgid[i];
			int iz = ev->iz[i];
			int izsub = ev->izsub[i];
			double x = ev->x[i];
			double y = ev->y[i];
			double z = ev->z[i];
			double px = ev->px[i];
			double py = ev->py[i];
			double pz = ev->pz[i];
			Data data = {iz,izsub,x,y,z,px,py,pz};
			datavector.push_back(data);
		}
        cout << typeid(datavector).name() << endl;
    int n = datavector.size();
    int i, j, min_idx;
    for (i = 0; i < n-1; i++)
    {
        min_idx = i;
        for (j = i+1; j < n; j++)
            if (datavector[j].iz_ < datavector[min_idx].iz_)
                min_idx = j;
        if (min_idx != i) {
            swap(&datavector[min_idx], &datavector[i]);
        }
    }
	int idxarray[1000];
	int izvalue;
	int izlast = -1;
        // checking iz_ ordering
		for (int idx=0;idx<nhits;idx++) {
//		    cout << idx << " " << datavector[idx].iz_ << " " << datavector[idx].z_ << endl;
		    izvalue = datavector[idx].iz_;
		    if(izvalue > izlast){
			idxarray[izvalue] = idx;
			izlast = izvalue;
		    }
		}
	// checking index array
	for(int i=0;i<1000;i++){
//		cout << idxarray[i] << " ";
	}
	cout << endl;
	for(int i=0;i<nhits;i++){ // Loop selects starting hit for basetrack (debugging
		if(datavector[i].izsub_!=0)continue; // Use only hits in the entrance to the emulsion film for start of basetrack
		int izbasetrack = datavector[i].iz_; // Get iz (index of idxarray)
		int idxlower = idxarray[izbasetrack];
		if(izbasetrack==999)int idxupper = nhits;
		else int idxupper = idxarray[izbasetrack+1];
		double x1 = datavector[i].x_;
		double y1 = datavector[i].y_;
		double z1 = datavector[i].z_;
		double px1 = datavector[i].px_;
		double py1 = datavector[i].py_;
		double pz1 = datavector[i].pz_;
		double zex = -695.35+izbasetrack*1.3; // Extrapolated z for microtrack linking
		double chi2min = 100000;
		int imin, jmin;
		double x1min,y1min,z1min,x2min,y2min,z2min,px1min,py1min,pz1min,px2min,py2min,pz2min,anglex,angley;
		for(int j=idxlower;j<idxupper;j++){ // Loop over end of basetrack
			if(datavector[j].izsub_!=1)continue; // Use only hits exiting the emulsion film for end of basetrack
			double x2 = datavector[j].x_;
			double y2 = datavector[j].y_;
			double z2 = datavector[j].z_;
			if((x2-x1)**2+(y2-y1)**2>1) continue; // Only consider exit microtracks close to entrance microtracks
			double px2 = datavector[j].px_;
			double py2 = datavector[j].py_;
			double pz2 = datavector[j].pz_;
			double dz1 = zex - z1; // distance from emulsion hit to middle of plastic base
			double dz2 = z2 - zex; // distance from emulsion hit to middle of plastic base
			double dx1 = dz1*px1/pz1; // dx1 = dz1*px1/pz1
			double dx2 = dz2*px2/pz2;
			double dy1 = dz1*py1/pz1;
			double dy2 = dz2*py2/pz2;
			double xex1 = x1+dx1;
			double yex1 = y1+dy1;
			double xex2 = x2+dx2;
			double yex2 = y2+dy2;
//			cout << "x1 = " << x1 << " y1 = " << y1 << " z1 = " << z1 << endl;
//			cout << "xex1 = " << xex1 << " yex1 = " << yex1 << " zex1 = " << zex << endl;
//			cout << "xex2 = " << xex2 << " yex2 = " << yex2 << " zex2 = " << zex << endl;
//			cout << "x2 = " << x2 << " y2 = " << y2 << " z2 = " << z2 << endl;
			double chi2 = (xex1-xex2)**2 + (yex1-yex2)**2; // chi2 defined via extrapolation mismatch
			if(chi2<chi2min){
				chi2min = chi2;
//				cout << "Decrease chi2min" << endl;
				imin = i;
				jmin = j;
				x1min = x1;
				y1min = y1;
				z1min = z1;
				x2min = x2;
				y2min = y2;
				z2min = z2;
				px1min = px1;
				py1min = py1;
				pz1min = pz1;
				px2min = px2;
				py2min = py2;
				pz2min = pz2;
			}
		}
//		cout << "Basetrack found: (i,j) = (" << imin << "," << jmin << ")" << endl;
		anglex = atan((x2min-x1min)/(z2min-z1min));
		angley = atan((y2min-y1min)/(z2min-z1min));
//		cout << "(" << x1min << "," << y1min << "," << z1min << ") to (" << x2min << "," << y2min << "," << z2min << ") anglex = " << anglex << " angley = " << angley << " (chi2 = " << chi2min << ")" << endl; // Print basetrack variables
		hx1->Fill(x1min);
		hy1->Fill(y1min);
		hz1->Fill(z1min);
		hx2->Fill(x2min);
		hy2->Fill(y2min);
		hz2->Fill(z2min);
		hpx1->Fill(px1min);
		hpy1->Fill(py1min);
		hpz1->Fill(pz1min);
		hpx2->Fill(px2min);
		hpy2->Fill(py2min);
		hpz2->Fill(pz2min);
		hanglex->Fill(anglex);
		hangley->Fill(angley);
		hchi2->Fill(chi2min);

	}
		hx1->Write();
		hy1->Write();
		hz1->Write();
		hx2->Write();
		hy2->Write();
		hz2->Write();
		hpx1->Write();
		hpy1->Write();
		hpz1->Write();
		hpx2->Write();
		hpy2->Write();
		hpz2->Write();
		hanglex->Write();
		hangley->Write();
		hchi2->Write();

		g->Close();
		cout << "FINISHED" << endl;
	}
}