void plot(TString variable){
	TString hname = TString("bg") + variable;
	TFile *f = new TFile("variables.root");
	TFile *g = new TFile("variables_bg.root");
	TH1D *hsig = f->Get(hname);
	TH1D *hbg = g->Get(hname);
	TCanvas *c = new TCanvas();
	double hsigintegral = hsig->Integral(0,2);
	double hbgintegral = hbg->Integral(0,2);
	hsig->Scale(1/hsigintegral);
	hbg->Scale(1/hbgintegral);
	hbg->SetLineColor(kRed);
	hbg->Draw();
	hsig->Draw("same");
	c->SaveAs(variable + TString(".png"));
}
void cut(TString variable){
	TString hname = TString("bg") + variable;
	TFile *f = new TFile("variables.root");
	TFile *g = new TFile("variables_bg.root");
	TH1D *hsig = f->Get(hname);
	TH1D *hbg = g->Get(hname);
	int cut = 0;
	int sigint = hsig->Integral(cut,hsig->GetNbinsX());
	cout << "Signal integral = " << sigint << endl;
	double bgint = hbg->Integral(cut,hbg->GetNbinsX());
	cout << "Background integral = " << bgint << endl;
	double signif = sigint/sqrt(bgint);
}
void cutplots(){
	plot(TString("alpha"));
//	plot(TString("angle"));
//	plot(TString("anglex"));
//	plot(TString("angley"));
//	plot(TString("ip"));
}