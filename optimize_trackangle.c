#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include<TLeaf.h>
#ifndef FnuG4_h
#define FnuG4_h
class Hit: public TObject{
	public:
	int iz, izsub, pdgid, id, idParent;
	float charge, x, y, z, px, py, pz, e1, e2, len, edep;
	ClassDef(Hit, 1);
};
class FnuG4 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        e_beam;
   Int_t           id_beam;
   Double_t        x_beam;
   Double_t        y_beam;
   Int_t           pdgnu_nuEvt;
   Int_t           pdglep_nuEvt;
   Double_t        Enu_nuEvt;
   Double_t        Plep_nuEvt;
   Int_t           cc_nuEvt;
   Double_t        x_nuEvt;
   Double_t        y_nuEvt;
   Double_t        z_nuEvt;
   Int_t           nhits;
   Int_t           chamber_count;
   Int_t           chamber[846048];   //[chamber_count]
   Int_t           iz_count;
   Int_t           iz[846048];   //[iz_count]
   Int_t           izsub_count;
   Int_t           izsub[846048];   //[izsub_count]
   Int_t           pdgid_count;
   Int_t           pdgid[846048];   //[pdgid_count]
   Int_t           id_count;
   Int_t           id[846048];   //[id_count]
   Int_t           idParent_count;
   Int_t           idParent[846048];   //[idParent_count]
   Int_t           charge_count;
   Double_t        charge[846048];   //[charge_count]
   Int_t           x_count;
   Double_t        x[846048];   //[x_count]
   Int_t           y_count;
   Double_t        y[846048];   //[y_count]
   Int_t           z_count;
   Double_t        z[846048];   //[z_count]
   Int_t           px_count;
   Double_t        px[846048];   //[px_count]
   Int_t           py_count;
   Double_t        py[846048];   //[py_count]
   Int_t           pz_count;
   Double_t        pz[846048];   //[pz_count]
   Int_t           e1_count;
   Double_t        e1[846048];   //[e1_count]
   Int_t           e2_count;
   Double_t        e2[846048];   //[e2_count]
   Int_t           len_count;
   Double_t        len[846048];   //[len_count]
   Int_t           edep_count;
   Double_t        edep[846048];   //[edep_count]

   // List of branches
   TBranch        *b_row_wise_branch;   //!

	TObjArray *particles;
   


   FnuG4(TTree *tree=0);
   virtual ~FnuG4();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   void Print();
   
};

FnuG4::FnuG4(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("FASERnu_numu.dump._001-PILEUP.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("FASERnu_numu.dump._001-PILEUP.root");
      }
      f->GetObject("FASERnu",tree);

   }
   Init(tree);
   
   particles = new TObjArray;
   particles->SetOwner(1);
}

FnuG4::~FnuG4()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}
Int_t FnuG4::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   
   delete particles;
   particles = new TObjArray;
   particles->SetOwner(1);
   
   int ret = fChain->GetEntry(entry);
   nhits = chamber_count;

   return ret;
}

void FnuG4::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("row_wise_branch", &e_beam, &b_row_wise_branch);


 
   fChain->GetLeaf("e_beam")->SetAddress( &e_beam);
   fChain->GetLeaf("id_beam")->SetAddress( &id_beam);
   fChain->GetLeaf("x_beam")->SetAddress( &x_beam);
   fChain->GetLeaf("y_beam")->SetAddress( &y_beam);
   fChain->GetLeaf("pdgnu_nuEvt")->SetAddress( &pdgnu_nuEvt);
   fChain->GetLeaf("pdglep_nuEvt")->SetAddress( &pdglep_nuEvt);
   fChain->GetLeaf("Enu_nuEvt")->SetAddress( &Enu_nuEvt);
   fChain->GetLeaf("Plep_nuEvt")->SetAddress( &Plep_nuEvt);
   fChain->GetLeaf("cc_nuEvt")->SetAddress( &cc_nuEvt);
   fChain->GetLeaf("x_nuEvt")->SetAddress( &x_nuEvt);
   fChain->GetLeaf("y_nuEvt")->SetAddress( &y_nuEvt);
   fChain->GetLeaf("z_nuEvt")->SetAddress( &z_nuEvt);
//   fChain->GetLeaf("chamber_count")->SetAddress( &nhits);
   fChain->GetLeaf("chamber_count")->SetAddress( &chamber_count);
   fChain->GetLeaf("chamber")->SetAddress(chamber);   //[chamber_count]
   fChain->GetLeaf("iz_count")->SetAddress( &iz_count);
   fChain->GetLeaf("iz")->SetAddress(iz);   //[iz_count]
   fChain->GetLeaf("izsub_count")->SetAddress( &izsub_count);
   fChain->GetLeaf("izsub")->SetAddress(izsub);   //[izsub_count]
   fChain->GetLeaf("pdgid_count")->SetAddress( &pdgid_count);
   fChain->GetLeaf("pdgid")->SetAddress(pdgid);   //[pdgid_count]
   fChain->GetLeaf("id_count")->SetAddress( &id_count);
   fChain->GetLeaf("id")->SetAddress(id);   //[id_count]
   fChain->GetLeaf("idParent_count")->SetAddress( &idParent_count);
   fChain->GetLeaf("idParent")->SetAddress(idParent);   //[idParent_count]
   fChain->GetLeaf("charge_count")->SetAddress( &charge_count);
   fChain->GetLeaf("charge")->SetAddress(charge);   //[charge_count]
   fChain->GetLeaf("x_count")->SetAddress( &x_count);
   fChain->GetLeaf("x")->SetAddress(x);   //[x_count]
   fChain->GetLeaf("y_count")->SetAddress( &y_count);
   fChain->GetLeaf("y")->SetAddress(y);   //[y_count]
   fChain->GetLeaf("z_count")->SetAddress( &z_count);
   fChain->GetLeaf("z")->SetAddress(z);   //[z_count]
   fChain->GetLeaf("px_count")->SetAddress( &px_count);
   fChain->GetLeaf("px")->SetAddress(px);   //[px_count]
   fChain->GetLeaf("py_count")->SetAddress( &py_count);
   fChain->GetLeaf("py")->SetAddress(py);   //[py_count]
   fChain->GetLeaf("pz_count")->SetAddress( &pz_count);
   fChain->GetLeaf("pz")->SetAddress(pz);   //[pz_count]
   fChain->GetLeaf("e1_count")->SetAddress( &e1_count);
   fChain->GetLeaf("e1")->SetAddress(e1);   //[e1_count]
   fChain->GetLeaf("e2_count")->SetAddress( &e2_count);
   fChain->GetLeaf("e2")->SetAddress(e2);   //[e2_count]
   fChain->GetLeaf("len_count")->SetAddress( &len_count);
   fChain->GetLeaf("len")->SetAddress(len);   //[len_count]
   fChain->GetLeaf("edep_count")->SetAddress( &edep_count);
   fChain->GetLeaf("edep")->SetAddress(edep);   //[edep_count]


   Notify();
}
Bool_t FnuG4::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void FnuG4::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t FnuG4::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

#endif
void optimize_trackangle(){
	FnuG4 *ev = new FnuG4();
	TFile *g = new TFile("quality.root","RECREATE");
	TH1D *h = new TH1D("quality","Quality",100,0,100);
for(int cut=51;cut<70;cut++){
	int tot = 0;
	long double sigtot = 0;
	long double bgtot = 0;
	int sig = 0;
	int bg = 0;
	long int sigacc = 0;
	int bgacc = 0;
	long int bgrej = 0;
	for(int iev=0; iev<23000	; iev++){ // 3202 is electron
		if(iev==12219||iev==13771||iev==21440)continue;
		bgacc = 0;
		ev->GetEntry(iev);
		int nhits = ev->nhits;
		cout << "[" << iev << "] nhits: " << nhits;		
		int count = 0;
		int event = 0;
		int nmaxvalue = 0;
		int izmaxvalue = 0;
		for(int i=0; i<nhits; i++){
			if(ev->izsub[i]!=0) continue; // There are two hits for 1 emulsion film. Ignore one of them.
			if(ev->charge[i]==0) continue; // ignore neutral particles (photons, neutral hadrons) at truth level
			event++;
			if(iev==3202) sigtot++;
			else bgtot++;
//==========ADDITIONAL CUTS===========//
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>cut1/1000.) continue; // ignore everything outside of 100 um cylinder (optimizing)
			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>1/100.*abs(ev->z[i]+700.)) continue; // ignore everything outside of 0.01 rad cone (optimal)
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])<0.0001*abs(ev->z[i]+700.)) continue; // muon hits are close to axis, ignore them (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]>0.05) continue; // Keep only hits with track slope < 50 mrad
			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]<0.0001*cut) continue; // Muon hits have almost normal tracks, ignore (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i])<1000) continue; // Keep only hits with energy > 100 MeV
			count++;
			if(iev==3202) sigacc++;
			else bgacc++;
		}
//	Once for each event
	tot += event;
	cout << " tot = " << tot;
	cout << " sigtot = " << sigtot;
	cout << " sigacc = " << sigacc;
	cout << " bgtot = " << bgtot;
	if(iev!=3202)bgrej += event-bgacc;
	cout << " bgrej(cumul) = " << bgrej << endl;
	}
	cout << "TOTAL HITS = " << tot << endl;
	cout << "ACCEPTED SIGNAL HITS = " << sigacc << endl;
//	cout << "ACCEPTED BACKGROUND HITS = " << bgacc << endl;
	cout << "REJECTED BACKGROUND HITS = " << bgrej << endl;
	cout << "TOTAL SIGNAL HITS = " << sigtot << endl;
	cout << "TOTAL BACKGROUND HITS = " << bgtot << endl;
	double sigfrac = sigacc/sigtot;
	double bgfrac = bgrej/bgtot;
	cout << "SIGNAL ACCEPTANCE FRACTION = " << sigfrac << endl;
	cout << "BACKGROUNND REJECTION FRACTION = " << bgfrac << endl;
	double quality = sigfrac*bgfrac;
	cout << "QUALITY = " << quality << endl;
	h->SetBinContent(cut,quality);
}
	h->Write();
	g->Close();
}
